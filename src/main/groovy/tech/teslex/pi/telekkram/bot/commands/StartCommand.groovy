package tech.teslex.pi.telekkram.bot.commands

import groovy.transform.CompileStatic
import tech.teslex.pi.telekkram.bot.TelekkramBot
import tech.teslex.pi.telekkram.bot.commands.base.TelekkramCommand

import java.util.regex.Matcher

@CompileStatic
class StartCommand extends TelekkramCommand {

	StartCommand(TelekkramBot bot) {
		super('start', bot)
	}

	@Override
	def execute(Map update, Matcher matcher) {
		bot.sendMessage('Welcome!')
	}
}

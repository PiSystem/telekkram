package tech.teslex.pi.telekkram.bot

import cn.nukkit.plugin.Plugin
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import tech.teslex.pi.telekkram.bot.commands.base.TelekkramCommand
import tech.teslex.pi.telekkram.utils.Loading
import tech.teslex.telegroo.Telegroo

import java.util.regex.Matcher

@CompileStatic
class TelekkramBot extends Telegroo {

	Plugin plugin

	boolean threadActive = false

	TelekkramBot(String token, Plugin plugin) {
		super(token)
		this.plugin = plugin
	}

	@Override
	void start() {
		plugin.logger.notice('Starting Telekkram bot..')

		catchException = { Exception e ->
			active = false
			threadActive = false
			throw e
		}
		threadActive = true

		plugin.logger.notice('Telekkram bot started..')
		super.start()
		threadActive = false
		plugin.logger.notice('Telekkram bot disabled.')
	}

	@Override
	Thread startAsync() {
		return super.startAsync()
	}

	@Override
	void stop() {
		plugin.logger.notice('Stopping Telekkram bot..')
		super.stop()

		while (threadActive) {
			Loading.roll()
			sleep 100
		}
	}

	@CompileDynamic
	def registerCommand(TelekkramCommand command) {
		onCommand(command.command, { Map u, Matcher m -> command.execute(u, m) })
	}
}


package tech.teslex.pi.telekkram.bot.commands.base

import groovy.transform.CompileStatic

import java.util.regex.Matcher

@CompileStatic
interface Command {
	def execute(Map update, Matcher matcher)
}
package tech.teslex.pi.telekkram.bot.commands.base

import groovy.transform.CompileStatic
import tech.teslex.pi.telekkram.bot.TelekkramBot

import java.util.regex.Matcher

@CompileStatic
abstract class TelekkramCommand implements Command {

	String command

	TelekkramBot bot

	TelekkramCommand(String command, TelekkramBot bot) {
		this.command = command
		this.bot = bot
	}

	abstract def execute(Map update, Matcher matcher)
}

package tech.teslex.pi.telekkram.bot.commands

import cn.nukkit.scheduler.Task
import tech.teslex.pi.telekkram.bot.TelekkramBot
import tech.teslex.pi.telekkram.bot.commands.base.TelekkramCommand
import tech.teslex.pi.telekkram.sender.TelekkramCommandEvent
import tech.teslex.pi.telekkram.sender.TelekkramCommandSender

import java.util.regex.Matcher

class CommandCommand extends TelekkramCommand {

	CommandCommand(TelekkramBot bot) {
		super('(.+)', bot)
	}

	@Override
	def execute(Map update, Matcher matcher) {
		def cmd = matcher[0][1] as String
		def username = update.message.from.username

		if (cmd in bot.plugin.config['accepted_commands_for_all']) {
			null
		} else if (!(update.message.from.id in bot.plugin.config['accepted_users'] || update.message.from.username in bot.plugin.config['accepted_users']) || (cmd in bot.plugin.config['denied_commands_for_users'])) {
			if ((bot.plugin.config['access_denied_message'] as String).empty) {
				return
			} else {
				bot.sendMessage((bot.plugin.config['access_denied_message'] as String), [parse_mode: 'Markdown'])
				return
			}
		}

		def msg = bot.sendMessage('*EXECUTING..*', [parse_mode: 'Markdown']).result

		def sender = new TelekkramCommandSender(username: username)
		def event = new TelekkramCommandEvent(sender, cmd)

		bot.plugin.server.pluginManager.callEvent(event)

		if (!event.cancelled) {
			bot.plugin.server.scheduler.scheduleTask(
					new Task() {
						@Override
						void onRun(int i) {
							bot.plugin.server.dispatchCommand(sender, cmd)

							if (sender.messages.trim().empty) {
								bot.editMessageText(msg.message_id, '*DONE*', [parse_mode: 'Markdown'])
								return
							}

							sender.messages = sender.messages.replaceAll(/§[0-9a-z]{1}/, '')

							def result = sprintf(bot.plugin.config['response_message_style'] as String, sender.messages)

							bot.editMessageText(msg.message_id, result, [parse_mode: 'Markdown'])
						}
					}
			)
		}
	}

}

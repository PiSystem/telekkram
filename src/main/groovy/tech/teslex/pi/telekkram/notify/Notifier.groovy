package tech.teslex.pi.telekkram.notify

import cn.nukkit.event.player.PlayerCommandPreprocessEvent
import cn.nukkit.event.player.PlayerDeathEvent
import cn.nukkit.event.player.PlayerJoinEvent
import cn.nukkit.event.player.PlayerQuitEvent
import tech.teslex.pi.annotations.PiOn
import tech.teslex.pi.telekkram.Telekkram

class Notifier {

	Telekkram telekkram
	List objects = []

	Notifier(Telekkram telekkram, List events) {
		this.telekkram = telekkram
		if ('join' in events) objects.add(new Join())
		if ('quit' in events) objects.add(new Quit())
		if ('death' in events) objects.add(new Death())
		if (!(telekkram.config['notify_on']['command']['commands'] as List).empty) objects.add(new Command())
	}

	class Death {
		@PiOn
		void death(PlayerDeathEvent e) {
			def to = telekkram.config['notify_on']['notify_chat']
			def s = (telekkram.config['notify_on']['event']['style'] as String)
					.replace('{PLAYER}', e.entity.player.name)
					.replace('{MESSAGE}', telekkram.server.language.translate(e.deathMessage))
					.replaceAll(/§[0-9a-z]{1}/, '')

			Thread.start {
				telekkram.bot.sendMessage(s, to, [parse_mode: 'Markdown'])
			}
		}
	}

	class Quit {
		@PiOn
		void quit(PlayerQuitEvent e) {
			def to = telekkram.config['notify_on']['notify_chat']
			def s = (telekkram.config['notify_on']['event']['style'] as String)
					.replace('{PLAYER}', e.player.name)
					.replace('{MESSAGE}', telekkram.server.language.translate(e.quitMessage))
					.replaceAll(/§[0-9a-z]{1}/, '')

			Thread.start {
				telekkram.bot.sendMessage(s, to, [parse_mode: 'Markdown'])
			}
		}
	}

	class Join {
		@PiOn
		void join(PlayerJoinEvent e) {
			def to = telekkram.config['notify_on']['notify_chat']
			def s = (telekkram.config['notify_on']['event']['style'] as String)
					.replace('{PLAYER}', e.player.name)
					.replace('{MESSAGE}', telekkram.server.language.translate(e.joinMessage))
					.replaceAll(/§[0-9a-z]{1}/, '')

			Thread.start {
				telekkram.bot.sendMessage(s, to, [parse_mode: 'Markdown'])
			}
		}
	}

	class Command {
		@PiOn
		void cmd(PlayerCommandPreprocessEvent e) {
			def f = (telekkram.config['notify_on']['command']['commands'] as List)
					.find { e.message.startsWith "/$it" }
			if (f) {
				def to = telekkram.config['notify_on']['notify_chat']
				def s = (telekkram.config['notify_on']['command']['style'] as String)
						.replace('{PLAYER}', e.player.name)
						.replace('{COMMAND}', e.message)

				Thread.start {
					telekkram.bot.sendMessage(s, to, [parse_mode: 'Markdown'])
				}
			}
		}
	}

}
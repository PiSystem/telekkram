package tech.teslex.pi.telekkram

import cn.nukkit.command.CommandSender
import cn.nukkit.plugin.PluginBase
import cn.nukkit.scheduler.AsyncTask
import cn.nukkit.scheduler.TaskHandler
import cn.nukkit.utils.Config
import groovy.transform.CompileStatic
import tech.teslex.pi.PiApi
import tech.teslex.pi.annotations.PiCommand
import tech.teslex.pi.annotations.PiOn
import tech.teslex.pi.telekkram.bot.TelekkramBot
import tech.teslex.pi.telekkram.bot.commands.CommandCommand
import tech.teslex.pi.telekkram.bot.commands.StartCommand
import tech.teslex.pi.telekkram.notify.Notifier
import tech.teslex.pi.telekkram.sender.TelekkramCommandEvent
import tech.teslex.pi.telekkram.sender.TelekkramCommandSender

@CompileStatic
class Telekkram extends PluginBase {

	Config config

	TelekkramBot bot

	TaskHandler botTaskHandler

	void onLoad() {
		initConfig()
		initBot()
	}

	void onEnable() {
		PiApi.registerCommands(this)
		PiApi.registerEvents(this)

		if (config['notify_on']['enable'])
			initEvents()

		botTaskHandler = server.scheduler.scheduleAsyncTask(this, new AsyncTask() {
			@Override
			void onRun() {
				bot.catchException = { Exception e ->
					bot.active = false
					bot.threadActive = false
					throw e
				}

				bot.start()
			}
		})

		logger.info('Enabled')
	}

	void onDisable() {
		bot.stop()
	}

	void initBot() {
		bot = new TelekkramBot(config['token'] as String, this)

		bot.registerCommand(new StartCommand(bot))
		bot.registerCommand(new CommandCommand(bot))
	}

	void initEvents() {
		def o = new Notifier(this, config['notify_on']['event']['events'] as List)
		o.objects.each {
			PiApi.registerEvents(it)
		}
	}

	void initConfig() {
		saveResource('config.yml')
		config = new Config(
				new File(dataFolder, 'config.yml'),
				Config.YAML,
				[
						token                    : 'ABCDE:12345',
						accepted_users           : [123456, 'expexes'],
						denied_commands_for_users: ['stop'],
						accepted_commands_for_all: ['list'],
						access_denied_message    : '*ACCESS DENIED*',
						response_message_style   : '*RESULT*\n%s',
						notify_on                : [
								enable     : false,
								notify_chat: '1234567',
								command    : [
										style   : '*!!!{PLAYER} USE /{COMMAND}!!!*',
										commands: ['op']
								],
								event      : [
										style : '*{MESSAGE}*',
										events: ['join']
								]
						]
				] as LinkedHashMap<String, Object>
		)

//		config.save()
	}

	@PiCommand(command = 'tkkit', usage = '/tkkit [start|stop|status]')
	def tkkit(CommandSender sender, String s, String[] args) {
		if (!args.size()) {
			sender.sendMessage server.commandMap.getCommand(s).usage
			return
		}

		if (args[0] == 'start') {
			bot.startAsync()
		} else if (args[0] == 'stop') {
			logger.notice('Stopping Telekkram bot..')
			bot.active = false
		} else if (args[0] == 'status') {
			sender.sendMessage(bot.active ? bot.threadActive ? 'Active' : 'Stopping' : 'Disabled')
		} else if (args[0] == 'reload-config') {
			reloadConfig()
		}
	}

	@PiOn
	void telekkramCmd(TelekkramCommandEvent e) {
		logger.warning "${(e.sender as TelekkramCommandSender).username}: /$e.command"
	}
}

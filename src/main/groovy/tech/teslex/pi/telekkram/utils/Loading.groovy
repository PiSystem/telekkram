package tech.teslex.pi.telekkram.utils

class Loading {

	private static boolean active = false
	private static int c = 0

	static void start(int sleeps = 100) {
		active = true

		while (active) {
			roll()

			sleep sleeps
		}
	}

	static void roll() {
		def ar = ['⬜️','⬜️','⬜️','⬜️','⬜️','⬜️','⬜️','⬜️','⬜️','⬜️',]
		ar[c] = '⬛️'
		def arx = ar.join('')

		pf(arx)
		flush()

		c++

		if (c == 10)
			c = 0
	}

	static void stop() {
		active = false
	}

	private static void pf(String some) {
		def bb = ''
		some.size().times { i ->
			bb += '\b'
		}
		printf "$bb%s", some
	}

	private static void flush() {
		System.out.flush()
	}
}

package tech.teslex.pi.telekkram.sender

import cn.nukkit.command.CommandSender
import cn.nukkit.event.HandlerList
import cn.nukkit.event.server.ServerCommandEvent

class TelekkramCommandEvent extends ServerCommandEvent {
	private static final HandlerList handlers = new HandlerList()

	static HandlerList getHandlers() {
		return handlers
	}

	TelekkramCommandEvent(CommandSender sender, String command) {
		super(sender, command)
	}
}

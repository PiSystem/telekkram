package tech.teslex.pi.telekkram.sender

import cn.nukkit.command.RemoteConsoleCommandSender
import cn.nukkit.lang.TextContainer

class TelekkramCommandSender extends RemoteConsoleCommandSender {

	String messages = ''
	String username = ''

	void sendMessage(String message) {
		message = server.language.translateString(message)
		messages += message + '\n'
	}

	void sendMessage(TextContainer message) {
		sendMessage(server.language.translate(message))
	}

	String getMessages() {
		return messages
	}

	String getName() {
		'Telekkram'
	}
}
